#springcloud_consul
基于consul的微服务治理

1. 安装consul:下载https://www.consul.io/downloads.html
2. 启动consul agent -dev，访问http://localhost:8500成功
3. 修改springcloud-consul-provider端口的方法，启动多个实例
4. 启动springcloud-consul-consumer，feign会随机调用提供者的某个实例