package com.yanshaohui.springcloud

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.CommandLineRunner
import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.cloud.client.discovery.EnableDiscoveryClient
import org.springframework.cloud.netflix.feign.EnableFeignClients

import com.yanshaohui.springcloud.feign.Provider01FeignClient

@SpringBootApplication
@EnableDiscoveryClient
@EnableFeignClients
class ConsulConsumerApplication implements CommandLineRunner{
	static void main(String[] args) {
		SpringApplication.run(ConsulConsumerApplication.class, args);
	}

	@Autowired
	Provider01FeignClient client;
	
	@Override
	void run(String... args) throws Exception {
		100.times({
			def provider = client.hello();
			println "No.$it:$provider";
			Thread.sleep(600);
		});
	}
}
