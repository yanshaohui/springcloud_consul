package com.yanshaohui.springcloud.feign

import org.springframework.cloud.netflix.feign.FeignClient
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestMethod

@FeignClient(value="CONSUL-PROVIDER1")
interface Provider01FeignClient {
	
	@RequestMapping(value="/a1",method=RequestMethod.GET)
	String hello();
}
