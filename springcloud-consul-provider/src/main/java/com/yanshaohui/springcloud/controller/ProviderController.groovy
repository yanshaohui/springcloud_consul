package com.yanshaohui.springcloud.controller

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.cloud.client.discovery.DiscoveryClient
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RestController

@RestController
class ProviderController {

	@Autowired
	DiscoveryClient discoveryClient;
	
	@GetMapping("/a1")
	def provider1(){
		def localService = discoveryClient.getLocalServiceInstance();
		println "Provider Server:$localService.host:$localService.port";
		return localService.host + ":" + localService.port;
	}
}
